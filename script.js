
// Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// DOM представляє наш сайт як набір вузлів - текстових, елементів, коментарів,  це те, що формує браузер з html коду. ДОМ має деревоподібну структуру, де є батьківські елементи і їх нащадки. Через джаваскрипт ми можемо маніпулювати вузлами DOM - створювати, редагувати, видаляти.


// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML повертає вміст елемета, будь-які інтервали та нащадки, тобто будьзякий текст, включаючи теги, які містяться в елементі.
// innerText повертає текст всередені елементу без нащадків і пробілів і не повертає прихований текст.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// через глобальний обʼект document.  і потім 2 напрямки пошуку
// - getElement(s)by - 
// - querySelector
// Який спосіб кращий? - залежить від особливостей проекту, якщо треба швидкість, то через  getElement(s)by, якщо гнучкість - querySelector, так як в цьому способі є можливість використання унікальних селекторів, але це може бути трошки довше по швидкості, але не є критичним. querySelector і querySelectorAll є новітніми і кращим варіантом для роботи з сучасним ДОМ.


// 4. Яка різниця між nodeList та HTMLCollection?

// nodeList - це колекція вузлів, яка повертається такими методами, як querySelectorAll  і містить будь-який тип вузла = елементи, текстові, скрипти і так далі.
// HTMLCollection -містить вузли, що скаладються тільки з html-елементів. Повертається такими методами, як getElementsBy... по класу, тегу, айді, імені. Також це жіива колекція, тобто автоматично оновлюється при зміні документа.



// Практичні завдання
//  1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
//  Використайте 2 способи для пошуку елементів. 
//  Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
 const feature = document.getElementsByClassName('feature')
 console.log(feature)
 const feature1 = document.querySelectorAll('.feature')
 console.log(feature1)

 const featureCopy = [...feature1]
featureCopy.forEach(element => {
    return element.setAttribute('align', 'center')
});



//  2. Змініть текст усіх елементів h2 на "Awesome feature".
const h2 = document.getElementsByTagName('h2')
const h2Arr = [...h2];
console.log(h2Arr)
h2Arr.forEach((elem) => {
    console.log(elem.innerHTML);
    console.log(elem.innerText);

return elem.textContent = 'Awesome feature'
})


//  3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
const featureTitle = document.querySelectorAll('.feature-title')
  console.log(featureTitle)
  const featureTitleCopy = [...featureTitle]
  featureTitleCopy.forEach((elem) => {
    elem.textContent= elem.textContent+'!'
    return  elem
  })
